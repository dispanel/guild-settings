// Dependencies
import * as express from "express";
import * as bodyParser from 'body-parser';
import * as routes from './routes';
import { server } from '../config/server';
import * as client from './services/client';

// Express app
const app = express();

// CORS
app.use((req, res, next) => {
  res.header('Access-Header-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Header-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// Body Parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

client.login().then(client => {
  // Routes
  app.use(routes);
});


// Start Webserver
const PORT = server.port || 3001;
app.listen(PORT, () => {
  console.log(`Server has started on port ${PORT}`);
});