// Dependencies
import * as express from 'express';
import * as name from './name';
import * as categories from './categories';

// Router
const router = express.Router();

// Routes
router.use(name);
router.use(categories);

// Export
export = router;