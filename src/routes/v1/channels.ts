// Dependencies
import * as express from 'express';
import * as ChannelsController from '../../controllers/v1/channels';

// Router
const router = express.Router();

// Route
router.route('/guilds/:guildID/settings/categories')
  .get(ChannelsController.bulkGet)
  .post(ChannelsController.post)
  .put(ChannelsController.bulkPut)
  .delete(ChannelsController.bulkDelete);

router.route('/guilds/:guildID/settings/categories/:categoryID')
  .get(ChannelsController.getOne)
  .put(ChannelsController.putOne)
  .delete(ChannelsController.deleteOne);

// Export
export = router;