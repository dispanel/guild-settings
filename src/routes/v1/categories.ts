// Dependencies
import * as express from 'express';
import * as CategoriesController from '../../controllers/v1/categories';

// Router
const router = express.Router();

// Route
router.route('/guilds/:guildID/settings/categories')
  .get(CategoriesController.bulkGet)
  .post(CategoriesController.post)
  .put(CategoriesController.bulkPut)
  .delete(CategoriesController.bulkDelete);

router.route('/guilds/:guildID/settings/categories/:categoryID')
  .get(CategoriesController.getOne)
  .put(CategoriesController.putOne)
  .delete(CategoriesController.deleteOne);

// Export
export = router;