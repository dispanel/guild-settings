// Dependencies
import * as express from 'express';
import * as NameController from '../../controllers/v1/name';

// Router
const router = express.Router();

// Route
router.route('/guilds/:guildID/settings/name')
  .get(NameController.get)
  .put(NameController.put);

// Export
export = router;