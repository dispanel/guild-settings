// Dependencies
import * as express from 'express';
import * as v1 from './v1';

// Router
const router = express.Router();

// Routes
router.use('/v1', v1);

// Export
export = router;  