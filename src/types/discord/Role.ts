export interface Role {
  color: number;
  createdTimestamp: number;
  guildID: string;
  id: string;
  members: string[];
  name: string;
  position: number;
  permissions: number;
  mentionable: boolean;
}