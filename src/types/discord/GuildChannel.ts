export interface GuildMember {
  id: string;
  name: string;
  position: number;
  createdTimestamp: number;
  guildID: string;
  permissionOverwrites: number;
}