export interface GuildMember {
  id: string;
  name: string;
  displayName: string;
  displayColor: number;
  highestRoleID: string;
  nick: string;
  roles: number[];
  joinedAt: number;
  infractions: string[];
}