import { Request, Response } from "express";
import * as bot from '../../services/client';

export function get(req: Request, res: Response) {
  const guildID: string = req.params.guildID;
  const guild = bot.client.guilds.get(guildID);
  if (guild) {
    res.send(guild.name);
  } else {
    res.send('Guild was undefined');
  }
}

export function put(req: Request, res: Response) {
  res.send('ok');
}