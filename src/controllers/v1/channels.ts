import { Request, Response } from "express";
import * as bot from '../../services/client';
import { Guild, PermissionOverwrites, GuildChannel } from "discord.js";
import { GuildCategory } from '../../types/discord/GuildCategory';

export function bulkGet(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  // Query
  const channelsID: string[] = req.query.channelsID instanceof Array ? req.query.channelsID : [];
  const type: string = typeof req.query.type === 'string' ? req.query.type : '';

  if (guildID) {
    const guild: Guild | undefined = bot.client.guilds.get(guildID);
    if (guild) {
      const channels = guild.channels.filter((channel) => 
        (channel.type === 'text' || channel.type === 'voice') 
        && channelsID 
          ? channelsID.includes(channel.id)
          : true
        && type 
          ? channel.type === type
          : true
        );
      res.send(channels);
    } else {
      res.send('Guild was undefined');
    }
  } else {
    res.status(400).send('Invalid params');
  }
}

export async function post(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  // Body params
  const channel: GuildChannel | undefined = req.body;

  if (guildID && channel) {
    const channelName: string = typeof(channel.name) === 'string' ? channel.name : '';
    const channelType: string = typeof(channel.type) === 'string' ? channel.type : '';
    if (channelName && (channelType === 'text' || channelType === 'voice')) {
      const guild: Guild | undefined = bot.client.guilds.get(guildID);
      if (guild) {
        if (guild.available) {
          const position: number = !isNaN(Number(channel.position)) ? Number(channel.position) : 0;
          try {
            const channel = await guild.channels.create(channelName, {position, type: channelType});
            res.status(201).send({title: 'New channel created', reference: channel.id});
          } catch (e) {
            res.status(500).send(e);
          }
        } else {
          res.status(500).send('Guild\'s not available');
        }
      } else {
        res.status(400).send('Guild doesn\'t exist');
      }
    } else {
      res.status(400).send('Body parameter `name` was not defined or channel type was not equal to \'text\' or \'voice\'');
    }
  } else {
    res.status(400).send('Invalid params');
  }
}

export function bulkPut(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';

  // Queries
  const channelsID: string[] = req.params.channelsID instanceof Array ? req.params.channelsID : [];

  // Body params
  const permissionOverwrites: PermissionOverwrites[] = req.body.permissionOverwrites instanceof PermissionOverwrites ? req.body.permissionOverwrites : [];
  const nsfw: boolean | undefined = typeof(req.body.nsfw) === 'boolean' ? req.body.nsfw : undefined;
  const parentID: string = typeof(req.body.parentID) === 'string' ? req.body.parentID : '';

  if (guildID && channelsID && (permissionOverwrites || typeof nsfw !== undefined || parentID)) {
    const guild = bot.client.guilds.get(guildID);
    if (guild) {
      const channels = guild.channels.filter(channel => (channel.type === 'text' || channel.type === 'voice') && channelsID.includes(channel.id));
      if (channels) {
        channels.forEach(channel => {
          if (permissionOverwrites) {
            channel.edit({permissionOverwrites});
          }
          if (nsfw) {
            channel.edit({nsfw});
          }
          if (parentID) {
            channel.edit({parentID});
          }
          res.status(200).send('Channels were updated');
        });
      } else {
        res.status(400).send('No channels were found to update');
      }
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}

export function bulkDelete(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const channelsID: string[] = req.query.channelID instanceof Array ? req.params.channelsID : [];
  const channelName: string = typeof(req.query.categoryName) === 'string' ? req.query.channelName : '';
  const channelPosition: number = typeof(req.query.channelPosition) === 'number' ? req.query.channelPosition : 0;
  const channelParentID: string = typeof(req.query.channelParentID) === 'string' ? req.query.channelParentID : '';

  if (guildID && (channelsID || channelName || channelPosition || channelParentID)) {
    const guild = bot.client.guilds.get(guildID);
    if (guild) {
      const channels = guild.channels.filter(channel => (channel.type === 'text' || channel.type === 'voice'));
      const filteredChannels = channels.filter(channel => {
          const isIDIncluded = channelsID.includes(channel.id);
          let nameEquals = true;
          let positionEquals = true;
          let parentIDEquals = true;
          if (channelName) {
            nameEquals = channel.name === channelName;
          }
          if (channelPosition) {
            positionEquals = channel.position === channelPosition;
          }
          if (channelParentID) {
            parentIDEquals = channel.parentID === channelParentID;
          }

          return isIDIncluded && nameEquals && positionEquals && parentIDEquals;
        }
      );
      filteredChannels.forEach(channel => channel.delete());
      res.status(200).send('Channels deleted');
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}

export function getOne(req: Request, res: Response) {
  // Params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const channelID: string = typeof(req.params.channelID) === 'string' ? req.params.channelID : '';

  if (guildID && channelID) {
    const guild: Guild | undefined = bot.client.guilds.get(guildID);
    if (guild) {
      
      const channels = guild.channels.filter((channel) => (channel.type === 'text' || channel.type === 'voice'));
      const channel = channels.get(channelID);
      if (channel) {
        res.send(channel);
      } else {
        res.status(400).send('Channel was not found');
      }
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid params');
  }
}

export function deleteOne(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const channelID: string = typeof(req.params.channelID) === 'string' ? req.params.channelID : '';

  if (guildID && channelID) {
    const guild = bot.client.guilds.get(guildID);
    if (guild) {
      const channels = guild.channels.filter(channel => (channel.type === 'text' || channel.type === 'voice'));
      const channel = channels.get(channelID);
      if (channel) {
        channel.delete();
      } else {
        res.status(400).send('Channel was not found');
      }
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}

export function putOne(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const channelID: string = typeof(req.params.channelID) === 'string' ? req.params.channelID : '';

  if(guildID && channelID) {
    const channelBody: GuildCategory = req.body;
    if(channelBody) {
      const guild: Guild | undefined = bot.client.guilds.get(guildID);
      if (guild) {
        if (guild.available) {
          const channels = guild.channels.filter((channel) => (channel.type === 'text' || channel.type === 'voice'));
          const channel = channels.get(channelID);
          if(channel) {
            const name: string = typeof(channelBody.name) === 'string' ? channelBody.name : channelBody.name; 
            const position: number = !isNaN(Number(channelBody.position)) ? Number(channelBody.position) : channel.position;

            channel.edit({name});
            if(typeof(position) === 'number') {
              channel.edit({position});
            }
            res.status(200).send('Channel has been updated');
          } else {
            res.status(400).send('Channel doesn\'t exist');
          }
        } else {
          res.status(500).send('Guild is not available');
        }
      } else {
        res.status(500).send('Guild was undefined');
      }
    } else {
      res.status(400).send('No body was included');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}