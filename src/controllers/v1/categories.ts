import { Request, Response } from "express";
import * as bot from '../../services/client';
import { Guild, PermissionOverwrites } from "discord.js";
import { GuildCategory } from '../../types/discord/GuildCategory';

export function bulkGet(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  // Query
  const categoriesID: string[] = req.query.categoriesID instanceof Array ? req.query.categoriesID : [];

  if (guildID) {
    const guild: Guild | undefined = bot.client.guilds.get(guildID);
    if (guild) {
      const categories = guild.channels.filter((channel) => 
        channel.type === 'category' 
        && categoriesID 
          ? categoriesID.includes(channel.id) 
          : true
        );
      res.send(categories);
    } else {
      res.send('Guild was undefined');
    }
  } else {
    res.status(400).send('Invalid params');
  }
}

export async function post(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const category: GuildCategory | undefined = req.body;

  if (guildID && category) {
    const categoryName: string = typeof(category.name) === 'string' ? category.name : '';
    if (categoryName) {
      const guild: Guild | undefined = bot.client.guilds.get(guildID);
      if (guild) {
        if (guild.available) {
          const position: number = !isNaN(Number(category.position)) ? Number(category.position) : 0;
          try {
            const channel = await guild.channels.create(categoryName, {position, type: 'category'});
            res.status(201).send({title: 'New category created', reference: channel.id});
          } catch (e) {
            res.status(500).send(e);
          }
        } else {
          res.status(500).send('Guild\'s not available');
        }
      } else {
        res.status(400).send('Guild doesn\'t exist');
      }
    } else {
      res.status(400).send('Body parameter `name` was not defined');
    }
  } else {
    res.status(400).send('Invalid params');
  }
}

export function bulkPut(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';

  // Queries
  const categoriesID: string[] = req.params.categoriesID instanceof Array ? req.params.categoriesID : [];

  // Body params
  const permissionOverwrites: PermissionOverwrites[] = req.body.permissionOverwrites instanceof PermissionOverwrites ? req.body.permissionOverwrites : [];

  if (guildID && categoriesID && permissionOverwrites) {
    const guild = bot.client.guilds.get(guildID);
    if (guild) {
      const categories = guild.channels.filter(channel => channel.type === 'category' && categoriesID.includes(channel.id));
      if (categories) {
        categories.forEach(category => {
          category.edit({permissionOverwrites});
          res.status(200).send('Categories were updated');
        });
      } else {
        res.status(400).send('No categories were found to update');
      }
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}

export function bulkDelete(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const categoriesID: string[] = req.query.categoriesID instanceof Array ? req.params.categoriesID : [];
  const categoryName: string = typeof(req.query.categoryName) === 'string' ? req.query.categoryName : '';
  const categoryPosition: number = typeof(req.query.categoryPosition) === 'number' ? req.query.categoryPosition : 0;
  const categoryParentID: string = typeof(req.query.categoryParentID) === 'string' ? req.query.categoryParentID : '';

  if (guildID && (categoriesID || categoryName || categoryPosition || categoryParentID)) {
    const guild = bot.client.guilds.get(guildID);
    if (guild) {
      const categories = guild.channels.filter(channel => channel.type === 'category');
      const filteredCategories = categories.filter(category => {
          const isIDIncluded = categoriesID.includes(category.id);
          let nameEquals = true;
          let positionEquals = true;
          let parentIDEquals = true;
          if (categoryName) {
            nameEquals = category.name === categoryName;
          }
          if (categoryPosition) {
            positionEquals = category.position === categoryPosition;
          }
          if (categoryParentID) {
            parentIDEquals = category.parentID === categoryParentID;
          }

          return isIDIncluded && nameEquals && positionEquals && parentIDEquals;
        }
      );
      filteredCategories.forEach(category => category.delete());
      res.status(200).send('Categories deleted');
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}

export function getOne(req: Request, res: Response) {
  // Params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const categoryID: string = typeof(req.params.categoryID) === 'string' ? req.params.categoryID : '';

  if (guildID && categoryID) {
    const guild: Guild | undefined = bot.client.guilds.get(guildID);
    if (guild) {
      
      const categories = guild.channels.filter((channel) => channel.type === 'category');
      const category = categories.get(categoryID);
      if (category) {
        res.send(category);
      } else {
        res.status(400).send('Category was not found');
      }
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid params');
  }
}

export function deleteOne(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const categoryID: string = typeof(req.params.categoryID) === 'string' ? req.params.categoryID : '';

  if (guildID && categoryID) {
    const guild = bot.client.guilds.get(guildID);
    if (guild) {
      const categories = guild.channels.filter(channel => channel.type === 'category');
      const category = categories.get(categoryID);
      if (category) {
        category.delete();
      } else {
        res.status(400).send('Category was not found');
      }
    } else {
      res.status(400).send('Guild was not found');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}

export function putOne(req: Request, res: Response) {
  // Path params
  const guildID: string = typeof(req.params.guildID) === 'string' ? req.params.guildID : '';
  const categoryID: string = typeof(req.params.categoryID) === 'string' ? req.params.categoryID : '';

  if(guildID && categoryID) {
    const categoryBody: GuildCategory = req.body;
    if(categoryBody) {
      const guild: Guild | undefined = bot.client.guilds.get(guildID);
      if (guild) {
        if (guild.available) {
          const categories = guild.channels.filter((channel) => channel.type === 'category');
          const category = categories.get(categoryID);
          if(category) {
            const name: string = typeof(categoryBody.name) === 'string' ? categoryBody.name : category.name; 
            const position: number = !isNaN(Number(categoryBody.position)) ? Number(categoryBody.position) : category.position;

            category.edit({name});
            if(typeof(position) === 'number') {
              category.edit({position});
            }
            res.status(200).send('Category has been updated');
          } else {
            res.status(400).send('Category doesn\'t exist');
          }
        } else {
          res.status(500).send('Guild is not available');
        }
      } else {
        res.status(500).send('Guild was undefined');
      }
    } else {
      res.status(400).send('No body was included');
    }
  } else {
    res.status(400).send('Invalid parameters were given');
  }
}