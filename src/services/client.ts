import axios from 'axios';
import { server as serverConfig } from '../../config/server';
import * as Discordjs from 'discord.js';

// Discord Client
let client: Discordjs.Client;

// Login to the bot
async function login(): Promise<Discordjs.Client> {
  // Request Bot Token
  return new Promise<Discordjs.Client>((resolve, reject) => {
    axios.get(serverConfig.apiGatewayURL + '/discord/token')
      .then(data => {
        // Bot Token
        const token: string = data.data as unknown as string;
        // Instantiate client
        client = new Discordjs.Client();
        // Login
        if (client !== undefined && client instanceof Discordjs.Client) {
          client.login(token)
            .then(() => {
              resolve(client);
            })
            .catch(err => {
              reject(err);
            });
        } else {
          reject('Client was undefined');
        }
      })
      .catch(err => {
        reject(err);
      });
  });
}

export { login, client };