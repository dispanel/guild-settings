import * as serverConfig from './server.json';

interface Server {
  port: number;
  apiGatewayURL: string;
}

const server: Server = serverConfig;

export { server };
